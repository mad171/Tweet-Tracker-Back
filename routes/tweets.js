/** SOCKET MANAGING STREAMS ABOUT TWEETS */

var io = require('socket.io')();

console.log('[SOCKET-BACK] : initializing ...');
require('dotenv').config();
const Twit = require('twit');

const twitterClient = new Twit({
    consumer_key: process.env.consumer_key,
    consumer_secret: process.env.consumer_secret,
    access_token: process.env.access_token,
    access_token_secret: process.env.access_token_secret,
    timeout_ms: 60 * 1000,  // optional HTTP request timeout to apply to all requests.
    strictSSL: true,     // optional - requires SSL certificates to be valid.
});

const streamMap = new Map(); // [(key, tagInfo), ... ] where tagInfo: [nbUsers, stream]


// SOCKET IO __________________________________________________________________________
io.on('connection', (socket) => {
    console.log('[SOCKET-BACK] : New user connected !!');

    // Subscribe to a TAG
    socket.on('subscribe', function (tag) {
        console.log('[SOCKET-BACK] : One user subscribing to ' + tag);
        if (streamMap.get(tag) === undefined || streamMap.get(tag)[0] === 0) {
            console.log('[SOCKET-BACK] : nouveau tag !!');
            let newStream = twitterClient.stream('statuses/filter', { track: tag });
            newStream.on('tweet', function (tweet) {
                newTweet = lightenTweet(tweet);
                io.to(tag).emit('new-tweet', newTweet);
                // console.log(tweet);
            });
            let tagInfo = [0, newStream];
            streamMap.set(tag, tagInfo);
        }
        socket.join(tag);
        let tagInfo = streamMap.get(tag);
        tagInfo[0] = tagInfo[0] + 1;
        streamMap.set(tag, tagInfo);
    });

    // UnSubscribe to a TAG
    socket.on('unsubscribe', function (tag) {
        console.log('[SOCKET-BACK] : One user unsubscribing to ' + tag);
        if (streamMap.get(tag) !== undefined) {
            socket.leave(tag);
            let tagInfo = streamMap.get(tag);
            tagInfo[0] = tagInfo[0] - 1;
            streamMap.set(tag, tagInfo);
            if (streamMap.get(tag)[0] === 0) {
                // Détruire le stream
                console.log('[SOCKET-BACK] : stream traking ' + tag + ' closed.');
                streamMap.get(tag)[1].stop();
            }
        }
    });
});

io.on('disconnection', () => {
    console.log('[SOCKET-BACK] : One user disconnected !!');
});



function lightenTweet(tweet) {
    let lightTweet = new Object();

    lightTweet.created_at = tweet.created_at;
    lightTweet.id = tweet.id;
    lightTweet.user = {
        id: tweet.user.id,
        name: tweet.user.name,
        avatar: tweet.user.profile_image_url_https
    }
    lightTweet.geo = tweet.geo;
    lightTweet.coordinates = tweet.coordinates;
    lightTweet.place = tweet.place;
    lightTweet.lang = tweet.lang;
    if (Object.keys(tweet).includes('extended_tweet')) {
        lightTweet.text = tweet.extended_tweet.full_text;
    }
    else if (Object.keys(tweet).includes('retweeted_status')) {
        if (Object.keys(tweet.retweeted_status).includes('extended_tweet')) {
            lightTweet.text = tweet.retweeted_status.extended_tweet.full_text;
        }
        else {
            lightTweet.text = tweet.text;
        }
    }
    else {
        lightTweet.text = tweet.text;
    }
    return lightTweet;
}

module.exports = io;

