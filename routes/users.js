var express = require('express');
var router = express.Router();

let lastSocketID = 0;

/* GET users listing. */
router.get('/newSocketId', function (req, res, next) {
  lastSocketID = lastSocketID + 1;
  res.json({
    socketID: lastSocketID,
  });
});

module.exports = router;
